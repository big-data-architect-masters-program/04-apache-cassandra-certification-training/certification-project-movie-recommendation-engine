# Certification Project: Movie Recommendation Engine (in-progress)
1. In Cassandra DB createa keyspace movies_keyspace:

```shell
CREATE KEYSPACE movies_keyspace
WITH replication={'class':'SimpleStrategy','replication_factor:3'};
```

2. Create a table movies with 3 columns using cluster by:

```shell
create table movies (
    title text,
    also_view_title text,
    count int,
    PRIMARY KEY (title, count)
)
WITH CLUSTERING ORDER BY (count DESC);
```

3. Populate the movies table using a web service simulating the users click and Parse this click data and insert into Cassandra DB as follows:

```java
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
 
public class InsertData {
 
  public static void main(String[] args) {
 try {
 
  Client client = Client.create();
 
  WebResource webResource = client
     .resource("http://localhost:8080/movie-project/movies");
 
  ClientResponse response = webResource.accept("application/json")
                   .get(ClientResponse.class);
 
  if (response.getStatus() != 200) {
     throw new RuntimeException("Failed : HTTP error code : "
   + response.getStatus());
  }
 
  String output = response.getEntity(String.class);
 
  // JSON Output received in String
  Object obj = new JSONParser().parse(output);
  String title = (String) jo.get("title");
  String also_viewed_title = (String) jo.get("also_viewed_title");
        String count = (Integer) jo.get("count");
  public void insertbookByTitle(Book book) {
  StringBuilder sb = new StringBuilder("INSERT INTO ")
  .append(movies).append("(ID, count,also_viwed_title,title) ")
  .append("VALUES (").append(count.incrementAndGet())
  .append(", '").append("title").append(", '").append("also_viewed_title").append(", '").append("count").append("');");
    String query = sb.toString();
    session.execute(query);
}
 
   } catch (Exception e) {
 
  e.printStackTrace();
 
   }
 
 }
}
 
public class CassandraConnector {
    private Cluster cluster;
    private Session session;
    public void connect(String node, Integer port) {
        Builder b = Cluster.builder().addContactPoint(node);
        if (port != null) {
            b.withPort(port);
        }
        cluster = b.build();
        session = cluster.connect();
    }
    public Session getSession() {
        return this.session;
    }
    public void close() {
        session.close();
        cluster.close();
    }
}
```

5. Insert data as follows:

```shell
INSERT INTO movies (title,also_viewed_title,count) VALUES
('Titanic','Avatar',2) ;

INSERT INTO movies (title,also_viewed_title,count) 
VALUES ('Titanic','Jurassic park',1) ;
```

6. The recommended movies (final results) through Query:

```java
public Optional<Movie> queryMovieByTitleAndYear(final String title)
{
   final ResultSet movieResults = client.getSession().execute(
      "SELECT * from movies_keyspace.movies WHERE title = ? ", title);
   final Row movieRow = movieResults.one();
   final Optional<Movie> movie =
        movieRow != null
      ? Optional.of(new Movie(
           movieRow.getString("title"),
          : Optional.empty();
   return movie;
}
```

7.Return all records of movies table:

```java
public Optional<Movie> queryMovieByTitleAndYear(final String title)
{
   final ResultSet movieResults = client.getSession().execute(
      "SELECT * from movies_keyspace.movies WHERE title = ? ", title);
   final Row movieRow = movieResults.one();
   final Optional<Movie> movie =
        movieRow != null
      ? Optional.of(new Movie(
           movieRow.getString("title"),
           movieRow.getInt("count"),
           movieRow.getString("also_viwed_title"),
          : Optional.empty();
   return movie;
}
```
